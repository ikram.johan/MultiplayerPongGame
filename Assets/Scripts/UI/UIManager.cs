
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ExitGames.Client.Photon;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using Photon.Pun;
using UnityEngine.UI;
public class UIManager : MonoBehaviourPunCallbacks
{
    #region SinglePlayer
    /*
    [SerializeField] TMP_Text player1ScoreText;
    [SerializeField] TMP_Text player2ScoreText;
    //[SerializeField] TMP_Text winningText;
    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {          
            return instance;
        }
    }
    public void Awake()
    {
        if (instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public string Player1Score
    {      
        set { player1ScoreText.text = value; }
    }
    public string Player2Score
    {
        set { player2ScoreText.text = value; }
    }
    //public string WinningText
    //{
    //    set { winningText.text = value; }
    //} */
    #endregion

    public GameObject PlayerOverviewEntryPrefab;
    public GameObject ScoreUIParent;
    [SerializeField] TMP_Text winningText;
    [SerializeField] Button leaveRoomButton;
    PhotonView photonView;
    private Dictionary<int, GameObject> playerListEntries;
    public string WinningText { get; set; }
    
    #region UNITY

    public void Awake()
    {
        photonView = GetComponent<PhotonView>();
        playerListEntries = new Dictionary<int, GameObject>();

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            GameObject entry = Instantiate(PlayerOverviewEntryPrefab);
            entry.transform.SetParent(ScoreUIParent.transform);
            entry.transform.localScale = Vector3.one;

            entry.GetComponent<TMP_Text>().text = string.Format("{0}:{1}",p.NickName, p.GetScore());

            playerListEntries.Add(p.ActorNumber, entry);
        }
    }

    #endregion

    #region PUN CALLBACKS
    
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        GameObject go = null;
        if (this.playerListEntries.TryGetValue(otherPlayer.ActorNumber, out go))
        {
            Destroy(playerListEntries[otherPlayer.ActorNumber]);
            playerListEntries.Remove(otherPlayer.ActorNumber);
        }
        PhotonNetwork.LoadLevel(0);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        GameObject entry;
        if (playerListEntries.TryGetValue(targetPlayer.ActorNumber, out entry))
        {
            entry.GetComponent<TMP_Text>().text = string.Format("{0}:{1}",targetPlayer.NickName, targetPlayer.GetScore());
            if (targetPlayer.GetScore()==5)
            {
                WinningText = "Winner :" + targetPlayer.NickName;
                ShowWinningText();
            }
        }
    }

    public void ShowWinningText()
    {
        Debug.Log(WinningText);
       // winningText.text = WinningText;
        photonView.RPC("ShowWinningTextRPC", RpcTarget.All);
    }
    [PunRPC]
    void ShowWinningTextRPC()
    {
        
        winningText.text = WinningText;
        leaveRoomButton.gameObject.SetActive(true);
    }

    public void OnPressLeaveGame()
    {
        PhotonNetwork.LeaveRoom();
    }
    #endregion
}
