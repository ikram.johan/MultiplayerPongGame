using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDeactivator : MonoBehaviour
{
    public void OnEnable()
    {
        StartCoroutine(TimerRoutine());
    }

    IEnumerator TimerRoutine()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
    public void OnDisable()
    {
        StopAllCoroutines();
    }
}
