using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Chat;
using ExitGames.Client.Photon;
using Photon.Pun;
using TMPro;
using UnityEngine.UI;
using Photon.Chat.Demo;
public class PhotonChatManager : MonoBehaviour,IChatClientListener
{
    
    ChatClient chatClient;
    private string userID="";
    [SerializeField] TMP_InputField textInputField;
    [SerializeField] TMP_Text textOutputField;
    private string channelToJoinOnConnect;

    // Start is called before the first frame update
    void Start()
    {
       
    }
    public void OnEnable()
    {
        ConnectToChatServer();
    }
    // Update is called once per frame
    void Update()
    {
        chatClient.Service();
    }

    public void ConnectToChatServer()
    {
        channelToJoinOnConnect = "TestChannel";
        userID = PhotonNetwork.LocalPlayer.NickName;
        chatClient = new ChatClient(this);
        chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion, new AuthenticationValues(userID));
    }
    public void DebugReturn(DebugLevel level, string message)
    {
        if (level == DebugLevel.ERROR)
        {
            Debug.LogError(message);
        }
        else if (level == DebugLevel.WARNING)
        {
            Debug.LogWarning(message);
        }
        else
        {
            Debug.Log(message);
        }
    }

    public void OnChatStateChange(ChatState state)
    {
        
    }

    public void OnConnected()
    {
        Debug.Log("Chat is connected");
        if (this.channelToJoinOnConnect != null && this.channelToJoinOnConnect.Length > 0)
        {
            this.chatClient.Subscribe(this.channelToJoinOnConnect);
        }
    }

    public void OnDisconnected()
    {
        Debug.Log("Chat is disconnected");
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        Debug.Log($"{channelName}:{senders}:{messages}");
        string msgs = "";
        for (int i = 0; i < senders.Length; i++)
        {
            msgs = string.Format("{0}{1}:{2} ", msgs, senders[i], messages[i]);
            textOutputField.text = textOutputField.text + "\r\n" + msgs;
        }
        

    }
    public void OnPrivateMessage(string sender, object message, string channelName)
    {
       
        Debug.Log($"{sender}:{message}:{channelName}");
        
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        Debug.Log("Subscribed");
        
    }

    public void OnUnsubscribed(string[] channels)
    {
       
    }

    public void OnUserSubscribed(string channel, string user)
    {
      
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        
    }

    public void OnDisable()
    {
        DisconnecChat();
    }

    public void DisconnecChat()
    {
        chatClient.Disconnect();
    }

    public void OnSendButtonPressed()
    {
        SendMessage("JOHAN", string.Empty);
    }

    public void SendMessage(string receipent, string message)
    {
        if (string.IsNullOrEmpty(message))
        {
            message = textInputField.text;
        }
        textInputField.text = string.Empty;
       
        //chatClient.SendPrivateMessage("JOHAN", message);
        chatClient.PublishMessage(channelToJoinOnConnect, message);
    }
}
