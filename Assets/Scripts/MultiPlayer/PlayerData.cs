using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;

public class PlayerData : MonoBehaviour
{
    public const string PLAYER_READY = "IsPlayerReady";
    public TMP_Text nameText;

    public int ownId;

    public TMP_Text readyText;
    public Button readyButton;
    bool isPlayerReady;
    private void Start()
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber!=ownId)
        {
            readyButton.gameObject.SetActive(false);
        }
        else
        {
            Hashtable initprops = new Hashtable() { { PLAYER_READY, isPlayerReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(initprops);

            readyButton.onClick.AddListener(() =>
            {
                Debug.Log("Player Ready=" + isPlayerReady);
                isPlayerReady = !isPlayerReady;
                Hashtable props = new Hashtable() { { PLAYER_READY, isPlayerReady } };
                PhotonNetwork.LocalPlayer.SetCustomProperties(props);
                if (PhotonNetwork.IsMasterClient)
                {
                    GameObject.FindObjectOfType<MPLauncher>().LocalPlayerPropertiesUpdated();
                }
            });

        }
       
    }
    public void Init(int id, string name)
    {
        ownId = id;
        nameText.text = name;
    }


    public void SetPlayerReady( bool isPlayerReady)
    {
        readyText.text = isPlayerReady ? "Yes!Ready." : "Ready?";
        readyButton.GetComponent<Image>().color=isPlayerReady ?Color.green : Color.yellow;
    }
}
