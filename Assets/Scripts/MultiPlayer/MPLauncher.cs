using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class MPLauncher : MonoBehaviourPunCallbacks
{
    public const string PLAYER_READY = "IsPlayerReady";
    public const string ROOM_PASS = "Password";
    [SerializeField] TMP_InputField playerNameInput;

    [SerializeField] TMP_InputField createRoomNameInput;
    [SerializeField]
    TMP_InputField createRoomPassword;
    [SerializeField] TMP_InputField joinRoomNameInput;
    [SerializeField]
    TMP_InputField joinRoomPassword;
    [SerializeField] TMP_Text messageText;
    [SerializeField] GameObject loginPanel;
    [SerializeField] GameObject lobbyPanel;
    [SerializeField] GameObject insideRoomPanel;
    [SerializeField] GameObject createRoomPanel;
    [SerializeField] GameObject joinRoomPanel;
    [SerializeField] GameObject playerListParent;
    [SerializeField] GameObject playerDataPrefab;
    [SerializeField] GameObject startGameButton;
    private Dictionary<int, GameObject> playerList;
    private Dictionary<string, RoomInfo> cachedRoomList;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        playerNameInput.text = "Player " + Random.Range(1000, 10000);
        cachedRoomList = new Dictionary<string, RoomInfo>();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player Connected to master");
        SetPanelActive(lobbyPanel.name);
       
    }
    public override void OnCreatedRoom()
    {

        Debug.Log("room created: " + PhotonNetwork.CurrentRoom.Name);
        
        Debug.Log("room prop pass =" + PhotonNetwork.CurrentRoom.CustomProperties[ROOM_PASS]);
    }
    public override void OnJoinedLobby()
    {
        Debug.Log("Player joined a lobby");
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        // ClearRoomListView();

        UpdateCachedRoomList(roomList);
        // UpdateRoomListView();
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Player joined a room");
       
        SetPanelActive(insideRoomPanel.name);
        if (playerList == null)
        {
            playerList = new Dictionary<int, GameObject>();
        }


        foreach (Player p in PhotonNetwork.PlayerList)
        {
            var tempObject = Instantiate(playerDataPrefab, Vector3.one, Quaternion.identity);

            tempObject.transform.SetParent(playerListParent.transform);
            tempObject.GetComponent<PlayerData>().Init(p.ActorNumber, p.NickName);
            
            Debug.Log(p.NickName);

            object isPlayerReady;
            if (p.CustomProperties.TryGetValue(PLAYER_READY, out isPlayerReady))
            {
                tempObject.GetComponent<PlayerData>().SetPlayerReady((bool)isPlayerReady);
            }

            playerList.Add(p.ActorNumber, tempObject);
        }
        startGameButton.gameObject.SetActive(CheckPlayersReady());

    }
    public override void OnLeftRoom()
    {
        SetPanelActive(lobbyPanel.name);

        foreach (GameObject entry in playerList.Values)
        {
            Destroy(entry.gameObject);
        }

        playerList.Clear();
        playerList = null;
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        GameObject tempObject = Instantiate(playerDataPrefab, Vector3.one,Quaternion.identity);
        tempObject.transform.SetParent(playerListParent.transform);
        tempObject.GetComponent<PlayerData>().Init(newPlayer.ActorNumber, newPlayer.NickName);

        playerList.Add(newPlayer.ActorNumber, tempObject);

        startGameButton.gameObject.SetActive(CheckPlayersReady());
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(playerList[otherPlayer.ActorNumber].gameObject);
        playerList.Remove(otherPlayer.ActorNumber);

        startGameButton.gameObject.SetActive(CheckPlayersReady());
    }
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            startGameButton.gameObject.SetActive(CheckPlayersReady());
        }
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        SetPanelActive(lobbyPanel.name);
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed : " + message);
        SetAlertMessageAndActive(message);
        SetPanelActive(lobbyPanel.name);
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed : " + message);
        SetAlertMessageAndActive(message);
        SetPanelActive(lobbyPanel.name);
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        Debug.Log("OnPlayerPropertiesUpdate is called");
        if (playerList == null)
        {
            playerList = new Dictionary<int, GameObject>();
        }

        GameObject entry;
        if (playerList.TryGetValue(targetPlayer.ActorNumber, out entry))
        {
            object isPlayerReady;
            if (changedProps.TryGetValue(PLAYER_READY, out isPlayerReady))
            {
                entry.GetComponent<PlayerData>().SetPlayerReady((bool)isPlayerReady);
            }
        }

        startGameButton.gameObject.SetActive(CheckPlayersReady());
    }

    public void LocalPlayerPropertiesUpdated()
    {
        startGameButton.SetActive(CheckPlayersReady());
    }
    public void OnLoginButtonPressed()
    {

        if (playerNameInput.text.Equals(""))
        {
            //Debug.LogError("Player name can't be empty");
            SetAlertMessageAndActive("Player name can't be empty");
        }
        else
        {
            Debug.Log("Login...");
            SetAlertMessageAndActive("Successfully logged in");
            PhotonNetwork.LocalPlayer.NickName = playerNameInput.text;
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    

    public void OnCreateRoomButtonPressed()
    {
        string roomName = createRoomNameInput.text;

        roomName = roomName.Equals("") ? "room" + Random.Range(1000, 5000) : roomName;
        Hashtable props = new Hashtable()
        {
            {ROOM_PASS, createRoomPassword.text.ToString()}
        };

        RoomOptions options = new RoomOptions() { CustomRoomProperties = props };
        string[] customRoomPropsForLobby = { ROOM_PASS };
        options.CustomRoomPropertiesForLobby = customRoomPropsForLobby;
        PhotonNetwork.CreateRoom(roomName,options,TypedLobby.Default);
    }
    public void OnJoinGameButtonPressed()
    {

        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }

    }
    public void OnLeaveRoomButtonPressed()
    {
        if (PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();
        }
    }
    public void OnJoinRoomButtonPressed()
    {
        if (cachedRoomList.Count == 0)
        {
            Debug.Log("No room found");
            SetAlertMessageAndActive("Room not found");
            return;
        }
        string pass;
        foreach (RoomInfo info in cachedRoomList.Values)
        {          
            if (info.Name==joinRoomNameInput.text)
            {
                Debug.Log("Room Found: " + info.Name);
                if (info.CustomProperties.Count==0)
                {
                    Debug.Log("NO properties found!");
                }
                else
                {
                    if (info.CustomProperties.ContainsKey(ROOM_PASS))
                    {
                        Debug.Log("Room is protected with pass");
                    }
                }
                pass = info.CustomProperties[ROOM_PASS].ToString();
                if (pass != null)
                {
                    Debug.Log(pass);
                    if ( pass==joinRoomPassword.text)
                    {
                        if (PhotonNetwork.InLobby)
                        {
                            PhotonNetwork.LeaveLobby();
                        }
                        PhotonNetwork.JoinRoom(info.Name);
                        Debug.Log("Joined in room");
                        SetAlertMessageAndActive("Joined in room"+info.Name);
                        break;
                    }
                    else
                    {
                        SetAlertMessageAndActive( "Wrong Password!");
                        joinRoomPassword.GetComponentInChildren<TMP_Text>().text = string.Empty;
                    }
                }             
            }
            else
            {
                Debug.Log("No room found");
                SetAlertMessageAndActive("Room not found");
            }
        }
    }

    void SetAlertMessageAndActive(string msg)
    {
        messageText.text = msg;
        messageText.gameObject.SetActive(true);
        
    }
    public void OnJoinRandomRoomPressed()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnRoomListButtonPressed()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
        Debug.Log("RoomListCOunt=="+cachedRoomList.Count);
    }
    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        Debug.Log("UpadteCacheRoomList Called");
        foreach (RoomInfo info in roomList)
        {
            // Remove room from cached room list if it got closed, became invisible or was marked as removed
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);
                }

                continue;
            }

            // Update cached room info
            if (cachedRoomList.ContainsKey(info.Name))
            {
                cachedRoomList[info.Name] = info;
            }
            // Add new room info to cache
            else
            {
                cachedRoomList.Add(info.Name, info);
            }
        }
    }
    public void OnBackButtonPressed()
    {
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }

        SetPanelActive(lobbyPanel.name);
    }
    public void OnStartButtonPressed()
    {
        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel("PongGameScene");
    }
    private bool CheckPlayersReady()
    {
        Debug.Log("CheckPlayerREady is Called");
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }
        object isPlayerReady;
        Debug.Log("Player Count==" + PhotonNetwork.PlayerList.Length);
        if (PhotonNetwork.PlayerList.Length == 1)
        {
            SetAlertMessageAndActive("Please wait for another player");
            return false;
        }
        foreach (Player player in PhotonNetwork.PlayerList)
        {           
            if (player.CustomProperties.TryGetValue(PLAYER_READY, out isPlayerReady))
            {
                Debug.Log("property exist");
                Debug.Log(PLAYER_READY + " : " + (bool)isPlayerReady);
                if (!(bool) isPlayerReady)
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        return true;
    }
    public void SetPanelActive(string panelName)
    {
        Debug.Log(panelName);
        loginPanel.SetActive(panelName == loginPanel.name);
        lobbyPanel.SetActive(panelName == lobbyPanel.name);
        createRoomPanel.SetActive(panelName == createRoomPanel.name);
        insideRoomPanel.SetActive(panelName == insideRoomPanel.name);
        joinRoomPanel.SetActive(panelName == joinRoomPanel.name);
    }
}
