using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    Rigidbody2D playerRigidBody;
    float movementY;
    Vector3 startPosition;
    VariableJoystick joystick;
    PhotonView photonView;
    // Start is called before the first frame update
    void Start()
    {
        playerRigidBody = GetComponent<Rigidbody2D>();
        photonView = GetComponent<PhotonView>();
        startPosition = gameObject.transform.position;
        if (photonView.IsMine)
        {
            joystick = GameObject.FindObjectOfType<VariableJoystick>().GetComponent<VariableJoystick>();
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                joystick.gameObject.SetActive(false);
            }
#if UNITY_STANDALONE
            joystick.gameObject.SetActive(false);
#endif
        }


    }

    // Update is called once per frame
    public void Update()
    {

        if (!photonView.AmOwner) return;

        if (!photonView.IsMine) return;
        //if (Application.platform==RuntimePlatform.Android)
        //{
        //    movementY = joystick.Vertical;
        //}
        movementY = Input.GetAxisRaw("Vertical");


    }

    public void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Move(joystick.Vertical);
            }
            else
            {
                Move(movementY);
            }

        }

    }
    private void Move(float moveInput)
    {

        playerRigidBody.velocity = new Vector2(0f, moveInput * speed);


    }
    public void ResetPosition()
    {
        gameObject.transform.position = startPosition;
    }
}
