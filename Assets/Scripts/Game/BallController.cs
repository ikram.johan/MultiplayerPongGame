using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
public class BallController : MonoBehaviour
{
    [SerializeField] float speed = 2f;
    Rigidbody2D ballRigidBody;
    GameManager gameManager;
    PhotonView photonView;
    Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();
        startPosition = transform.position;
        ballRigidBody = GetComponent<Rigidbody2D>();
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        MoveBall();
    }

    private void MoveBall()
    {
        if (ballRigidBody == null) return;
        int x = Random.Range(0, 2) == 0 ? -1 : 1;
        int y = Random.Range(0, 2) == 0 ? -1 : 1;
        ballRigidBody.velocity = new Vector2(speed * x, speed * y);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }
        if (collision.CompareTag("Goal1"))
        {
            //ballRigidBody.velocity = Vector2.zero;
            //Debug.Log("Hey!Its goal2");
            // gameManager.CountPlayer2Score();
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (!p.IsMasterClient)
                {
                    p.AddScore(1);
                    // Debug.Log("Score Updated=" + p.GetScore());
                }
            }
        }
        else if (collision.CompareTag("Goal2"))
        {
            //ballRigidBody.velocity = Vector2.zero;
            //Debug.Log("Hey!Its goal1");
            //gameManager.CountPlayer1Score();
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (p.IsMasterClient)
                {
                    p.AddScore(1);
                    // Debug.Log("Score Updated=" + p.GetScore());
                }
            }
        }
    }
    [PunRPC]
    public void ResetPosition()
    {
        transform.position = startPosition;
        MoveBall();
    }

    [PunRPC]
    public void OnGameEnd()
    {
        ballRigidBody.velocity = Vector2.zero;
        gameObject.SetActive(false);
    }
}
