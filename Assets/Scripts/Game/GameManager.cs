using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
public class GameManager : MonoBehaviourPunCallbacks
{
    public int winningScore;
    [SerializeField] Transform player1Transform;
    [SerializeField] Transform player2Transform;
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected");
        PhotonNetwork.LoadLevel(0);
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.Disconnect();
    }


    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("target player=" + targetPlayer.NickName + " Score: " + targetPlayer.GetScore());
            if (targetPlayer.GetScore() == winningScore)
            {
                // IsGameEnded = true;
                //if (FindObjectOfType<BallController>()!=null)
                {
                    GameObject.FindObjectOfType<BallController>().GetComponent<PhotonView>().RPC("OnGameEnd", RpcTarget.All);
                }             
            }
            else
            {
                GameObject.FindObjectOfType<BallController>().GetComponent<PhotonView>().RPC("ResetPosition", RpcTarget.All);
            }
        }
    }
    private void StartGame()
    {
        Debug.Log("StartGame");
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.Instantiate("PlayerBar", player1Transform.position, player1Transform.rotation, 0);
        }
        else
        {
            PhotonNetwork.Instantiate("PlayerBar", player2Transform.position, player2Transform.rotation, 0);
        }
    }
}
